/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-------------------------------------------*/

#include <stdio.h>

/*-------------------------------------------*/

extern double uc1, r1, c1, il1, usr1, umc1, unc1;
extern double uc2, r2, c2, il2, usr2, umc2, unc2;
extern double uc3, r3, c3, il3, usr3, umc3, unc3;
extern double rws, rwg;
extern double uao, uaos, uaom, uaon;
extern double *tblu;
extern double t, tb, uz;

/*-------------------------------------------*/

void showf( double d ) {
	fprintf( stderr, " %13.10f", d );
}

void showc1( void ) {
	fprintf( stderr, "c1: " );
	showf( il1 );
	showf( uc1 );
	showf( usr1 );
	showf( umc1 );
	fprintf( stderr, "\n" );
}

void showc2( void ) {
	fprintf( stderr, "c2: " );
	showf( il2 );
	showf( uc2 );
	showf( usr2 );
	showf( umc2 );
	showf( umc2-unc2 );
	fprintf( stderr, "\n" );
}

void showc3( void ) {
	fprintf( stderr, "c3: " );
	showf( il3 );
	showf( uc3 );
	showf( usr3 );
	showf( umc3 );
	showf( umc3-unc3 );
	fprintf( stderr, "\n" );
}

void showao( void ) {
	fprintf( stderr, "ao: " );
	showf( uao );
	showf( uaos );
	showf( uaom );
	showf( uaom-uaon );
	fprintf( stderr, "\n" );
}

void showtime(void){
	fprintf( stderr, "tb %21.19f $\n", tb );
}

void showinf(void){
//	fprintf( stderr, "        ib     " );
	fprintf( stderr, "        ub     " );
	fprintf( stderr, "        usr    " );
	fprintf( stderr, "        umax\n" );
}


void saveuc1( void ) {
	fprintf( stderr, "c1: " );
	showf( il1 );
	showf( uc1 );
	showf( usr1 );
	showf( umc1 );
	fprintf( stderr, "\n" );
}

/*-------------------------------------------*/
