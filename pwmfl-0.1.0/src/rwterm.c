/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* ------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>  //exit
#include <stdint.h>  //types
#include <string.h>  //strncpy
#include <termios.h>
#include <unistd.h>  //exit

#include <fcntl.h>   //exitfile decrs

#include <sys/stat.h>
#include <sys/types.h>

#include <sys/wait.h>
#include <sys/ioctl.h>
#include <signal.h>

#include "timeutil.h"

/* ------------------------------------------------- */

void dtftim( FILE * f );
void dtfpkpk( FILE * f );
void dtfpktotim( FILE * f );

void qwit ( int ole );
int readklawa(void);
void showdane(void);
void showdanestr( FILE * filestr );
void showdanehtm( FILE * filestr );
void runptl( void );
void setpwmrel( void );

char * getnwathever (char *gdzie, void *c, int max );

void elemit( double T );
void elemawf( double wsp );
	
/* ------------------------------------------------- */

extern double uc1, r1, c1, il1, usr1, umc1, unc1;
extern double uc2, r2, c2, il2, usr2, umc2, unc2;
extern double uc3, r3, c3, il3, usr3, umc3, unc3;
extern double uc4, r4, c4, il4, usr4, umc4, unc4;
extern double errormax, wsp, wspt, wspcc, wspi, period;
extern double t, tb, uz;

extern void (*wskfun)(void);
extern void (*tblfun[]);

extern uint16_t multiplepwm;
extern uint32_t maxtimsym;
extern uint32_t _CLKGLOB;
extern int64_t ptl;

extern uint32_t uppwm;
extern uint32_t realuppwm;
extern uint32_t mainpwm;
extern uint32_t realpwm;

extern uint16_t nrtest;

extern char basepath[128];
extern char namefilter[64];
extern char pathfilter[384];
extern char pathpkpk[384];
extern char pathpktotim[384];

/* ------------------------------------------------- */

void savetohtml( FILE * plik );
FILE * starthtml( char *name );
void * endhtml( FILE * plik );
void gnuplotinf( void );

FILE * plikhtm;
char namehtm[] = "pwm";
uint16_t nmbplikhtm;
uint16_t nmbcalc;
uint16_t gnuplotexist;

uint8_t gnuplotterm;
char gnuplotpicext[6];

enum GNUPLOT {
	G_NULL, G_PNG, G_JPEG, G_PNGCAIRO
};

/* ------------------------------------------------- */

static double wspfl;
static uint8_t writedatahtm;

/* ------------------------------------------------- */

static int col, row, kolor, bkolor, rozj, invers , manuall, manhtm;

/* ------------------------------------------------- */

enum POINTTYPE {
	P_FILTR,
	P_T,
	P_I,
	P_IFL,
	P_MULTPWM,
	P_TIMES,
	P_CKLPWM,
	P_MANHA,
	P_MANUA,
	P_R1,
	P_R2,
	P_R3,
	P_R4,
	P_C1,
	P_C2,
	P_C3,
	P_C4,
	P_END
};

/* ------------------------------------------------- */

static char *namelem[] = {
	"FILTR",
	"T    ",
	"I    ",
	"IFL  ",
	"MPWM ",
	"TIMES",
	"CPWM ",
	"MANHA",
	"MANUA",
	"R1   ",
	"R2   ",
	"R3   ",
	"R4   ",
	"C1   ",
	"C2   ",
	"C3   ",
	"C4   "
};

#define SIZ_STR_NAMELEM (sizeof(namelem)/sizeof(*namelem))

static uint16_t poselem[P_END][3];

/* ------------------------------------------------- */

enum FILTERTYPE {
	FL_1_RC,
	FL_2_RC,
	FL_3_RC,
	FL_3_RC_BUF,
	FL_SALLENKEY,
	FL_RC_SALLENKEY,
	FL_4TH_1_OPAMP,
	FL_2_SALLENKEY,
	FL_END
};

static char *fltr[] = {
	"1xRC         ",
	"2xRC         ",
	"3xRC         ",
	"3xRC_BUF     ",
	"SALLEN-KEY   ",
	"RC_SALLEN-KEY",
	"4th_1xOPAMP  ",
	"2xSALLEN-KEY "
};

static char *fltrfile[] = {
	"1xRC",
	"2xRC",
	"3xRC",
	"3xRC-BUF",
	"SK",
	"RC-SK",
	"4th-1xOP",
	"2xSK"
};


#define SIZ_STR_FLTR (sizeof(fltr)/sizeof(*fltr))

static uint8_t typefilter;
static uint8_t point;

/* ------------------------------------------------- */

#define SAVE_CUR(WHERE) fprintf(WHERE, "%c[s", 0x1b );
#define REST_CUR(WHERE) fprintf(WHERE, "%c[u", 0x1b );
#define CLR_FORM(WHERE) fprintf(WHERE, "%c[%dm", 0x1b, 0 );
#define GOTO_CURS(WHERE,x,y) fprintf(WHERE, "%c[%d;%dH" , 0x1b, y, x );

#define KEY_LEFT_STR   "[D\0"
#define KEY_RIGHT_STR  "[C\0"
#define KEY_UP_STR     "[A\0"
#define KEY_DOWN_STR   "[B\0"

/* ------------------------------------------------- */

void clrline(FILE * str){
	int j;
	for( j=0; j<col; ++j ) fprintf(str," ");
}

void clrscrm( FILE * str){
	int i;
	GOTO_CURS(stderr, 0, 0 );
	for( i=0; i<row; ++i ){
		clrline(str);
		fprintf(str,"\n");
	}
	GOTO_CURS(stderr, 0, col-1 );
}

/* ------------------------------------------------- */

void initelem(void){
	
	int i,j,x,y,h;
	
	for( h=1, y=2, i=0; i<4; ++i, y+=1 ){
		for( x=2, j=0; j<4; ++j, x+=16, ++h ){
			if( h >= P_END ) break;
			poselem[h][0] = x;
			poselem[h][1] = y;
			poselem[h][2] = 0;
		}
	}
	
	poselem[0][0] = 5;
	poselem[0][1] = 1;
	poselem[0][2] = 0;
	poselem[point][2] = 1;
}

/* ------------------------------------------------- */

void showelem1( double v, uint8_t typ ){
	char buf[16];
	GOTO_CURS(stderr,poselem[typ][0],poselem[typ][1]);
	
	if( poselem[typ][2]) fprintf(stderr, ">");
	else fprintf(stderr, " ");
	
	fprintf(stderr, "%s", namelem[typ] );
	if( v > 9999999 ) sprintf( buf, "  big  " );
	else snprintf(buf, 12, "%7.3f", v );
	buf[7] = 0;
	fprintf(stderr, "%s  ", buf );
}

/* ------------------------------------------------- */

void showelemstr( char * str, uint8_t typ ){
	
	GOTO_CURS(stderr,poselem[typ][0],poselem[typ][1]);
	if( poselem[typ][2]) fprintf(stderr, ">");
	else fprintf(stderr, " ");
	fprintf(stderr, "%s", str );
}

/* ------------------------------------------------- */

void showelemall( void ){
	
	SAVE_CUR(stderr);
	
	showelemstr( fltr[typefilter], P_FILTR );
	showelem1( wspt            , P_T );
	showelem1( wsp             , P_I );
	showelem1( wspfl           , P_IFL );
	showelem1( multiplepwm     , P_MULTPWM );

	showelem1( maxtimsym/1000  , P_TIMES );
	showelem1( _CLKGLOB        , P_CKLPWM );
	
	if(manhtm) showelemstr( "ADD-a   "      , P_MANHA );
	else showelemstr( "ALL SAVE"      , P_MANHA );
	
	if(manuall) showelemstr( "MANUAL   "      , P_MANUA );
	else showelemstr( "AUTOMATIC"      , P_MANUA );
	
	showelem1( r1, P_R1 );
	showelem1( r2, P_R2 );
	showelem1( r3, P_R3 );
	showelem1( r4, P_R4 );
	showelem1( c1, P_C1 );
	showelem1( c2, P_C2 );
	showelem1( c3, P_C3 );
	showelem1( c4, P_C4 );
	
	GOTO_CURS(stderr,2,6);
	showdanestr( stderr );
	
	CLR_FORM(stderr);
	REST_CUR(stderr);
}

/* ------------------------------------------------- */
/* terminal size */

int getxyterm( void ) {

	struct winsize yxyy;
	int u;

	u = ioctl( 0, TIOCGWINSZ, &yxyy );

	col = yxyy.ws_col;
	row = yxyy.ws_row;

	return u;
}


/* ------------------------------------------------- */
/* funkcja pomocnicza, czyta maks 9 znakuw bez czasu
  do momentu braku czegos w bufie */

static struct termios oldsett, newsett;

int czytajbezczasu ( char * bu ) {

	int i = 0;
	int o = 0;

	newsett.c_cc[VMIN] = 0;
	tcsetattr( 0, TCSANOW, &newsett );

	for ( i=0; i<9; i++ ) {
	  o = read ( 0, bu, 1 ); 
	  if ( o == 0 ) break;
	  bu++;
	}

	*bu = '\0';

	newsett.c_cc[VMIN] = 1;
	tcsetattr( 0, TCSANOW, &newsett );

	return i;
}

/* ------------------------------------------------- */

void atrterm( void ){
	
	/* sygnaly */
	signal ( SIGINT, qwit );
	signal ( SIGQUIT, qwit );
	signal ( SIGTERM, qwit );
	
	tcgetattr( 0, &oldsett );
	newsett = oldsett;
	
	newsett.c_lflag &= ~( ICANON | ECHO );  
	newsett.c_cc[VMIN] = 1;
	newsett.c_cc[VTIME] = 0;

	tcsetattr( 0, TCSANOW, &newsett );
}

/* ------------------------------------------------- */

void qwit ( int ole ) {
	tcsetattr( 0, TCSANOW, &oldsett );
	plikhtm = endhtml( plikhtm );
	fprintf (stderr, "\nexit %d\n", ole );
	exit (0);
}

/* ------------------------------------------------- */
/* man console_codes */

void showsomethng(void){
	/* zachowaj gdzie  kursor */
	fprintf(stderr, "%c[s", 0x1b );
	
	fprintf(stderr, "%c[%dm%c[%dm", 0x1b, 32, 0x1b, 40 );
	//fprintf(stderr, "%c[%dm", 0x1b, 33 );
   fprintf(stderr, "%c[%d;%dH" , 0x1b, 0, 0 );
	if(invers)fprintf(stderr, "%c[%dm", 0x1b, 7 ); //invert
	//fprintf(stderr, "%c[%dm", 0x1b, 1 ); //light (bold)
	fprintf(stderr, "              %u %u                       \n", col, row );
	fprintf(stderr, "                                          \n" );
	fprintf(stderr, "%c[%dm", 0x1b, 0 );
	
	/* odtwurz */
	fprintf(stderr, "%c[u", 0x1b );

	/* kasuj formatowanie */
	fprintf(stderr, "%c[%dm", 0x1b, 0 );
}

/* ------------------------------------------------- */

void elemval( void ){
	elemit( wspt );
	if( typefilter < 4 ) elemawf( wsp ); /* proportion rc to other */
	if( typefilter == FL_RC_SALLENKEY ) {
		r2  *= wsp; r3  *= wsp; r4  *= wsp;
		c2  /= wsp; c3  /= wsp; c4  /= wsp;
		r3  *= wspfl; r4  *= wspfl*wspfl;
		c3  /= wspfl; c4  /= wspfl*wspfl;
	}
	if( typefilter == FL_SALLENKEY ){
		r2  *= wspfl; r3  *= wspfl; r4  *= wspfl;
		c2  /= wspfl; c3  /= wspfl; c4  /= wspfl;
	}
	if( typefilter == FL_4TH_1_OPAMP ){
		r2  *= wspfl; r3  *= wspfl*wspfl; r4  *= wspfl*wspfl*wspfl;
		c2  /= wspfl; c3  /= wspfl*wspfl; c4  /= wspfl*wspfl*wspfl;
	}
	if( typefilter == FL_2_SALLENKEY ){
		r2  *= wspfl; r4  *= wspfl;
		c2  /= wspfl; c4  /= wspfl;
	}
}

/* ------------------------------------------------- */

void testterm(void){
	
	getxyterm();
	atrterm();

	kolor = 37;
	bkolor = 40;
	rozj = 0;
	invers = 0;
	
	clrscrm(stderr);
	//showsomethng();

	writedatahtm = 0;
	typefilter = 0;
	point = 0;
	wspfl = 1;

	plikhtm = NULL;
	gnuplotexist = 0;
	nmbplikhtm = 0;
	manhtm = 0;
	manuall = 0;
	nmbcalc = 0;
	
	elemval();
	initelem();
	gnuplotinf();
		
	wskfun = tblfun[typefilter];
	clrscrm(stderr);
	
	while(1){

		if(!manuall) elemval();
			
		if(!manhtm){
			if( writedatahtm ){
				if(!plikhtm) plikhtm = starthtml( namehtm );
				savetohtml( plikhtm );
			}
			writedatahtm = 0;
		}
		
		if(nmbcalc>14) plikhtm = endhtml( plikhtm );
		
		showelemall();
		readklawa();
		wskfun = tblfun[typefilter];
	}
	
	tcsetattr( 0, TCSANOW, &oldsett );
	fprintf (stderr, "\nret fun\n" );
}

/* ------------------------------------------------- */

double getvalue( char * str ){
	
	double v;
	int dz, vint;
	
	v = 0;
	vint = 0;
	dz = 1;
	
	for( ; *str!=0 && *str!='.'; ++str )
		vint = (vint*10) + (*str-'0');
	
	if( *str == 0 ) return (double)vint;
	
	for( ++str; *str!=0 && *str!='.'; ++str, dz*=10 )
		vint = (vint*10) + (*str-'0');
	
	v = vint;
	v /= dz;
	
	return v;
}

/* ------------------------------------------------- */

int readklawa(void){

	static char strvalue[8] = { 0 };
	static uint8_t sizstrvalue = 0;
	uint8_t znak;
	int8_t znesc;
	double changevalue;
	double newvalue;
	char sekesc[11];
	
	znak = 0;
	znesc = 0;
	changevalue = 0;
	
	if( 1 != read ( 0, &znak, 1 ) ) return 0;
	
	if( znak == 10 ){
		fprintf (stderr, "\r             \r" );
		newvalue = getvalue( strvalue );
		
		switch(point){
			
			case P_FILTR:
				break;
			
			case P_T:
				if( ( newvalue>=0.1 ) && ( newvalue<10000 ) )
					wspt = newvalue;
				break;
			
			case P_I:
				if( ( newvalue>=0.1 ) && ( newvalue<10000 ) )
					wsp = newvalue;
				break;
			
			case P_IFL:
				if( ( newvalue>=0.1 ) && ( newvalue<10000 ) )
					wspfl = newvalue;
				break;
			
			case P_TIMES:
				if( ( newvalue>=1 ) && ( newvalue<1000 ) )
					maxtimsym = 1000 * newvalue;
				break;
				
			case P_CKLPWM:
				if( ( newvalue>=1 ) && ( newvalue<10000 ) )
					_CLKGLOB = newvalue;
				setpwmrel();
			break;
			
			case P_R1:
				if( ( newvalue>0.1 ) && ( newvalue<1000000 ) )
					r1 = newvalue;
				break;
				
			case P_R2:
				if( ( newvalue>0.1 ) && ( newvalue<1000000 ) )
					r2 = newvalue;
				break;
				
			case P_R3:
				if( ( newvalue>0.1 ) && ( newvalue<1000000 ) )
					r3 = newvalue;
				break;
			
			case P_R4:
				if( ( newvalue>0.1 ) && ( newvalue<1000000 ) )
					r4 = newvalue;
				break;
			
			case P_C1:
				if( ( newvalue>=0.001 ) && ( newvalue<1000 ) )
					c1 = newvalue;
				break;
				
			case P_C2:
				if( ( newvalue>=0.001 ) && ( newvalue<1000 ) )
					c2 = newvalue;
				break;
				
			case P_C3:
				if( ( newvalue>=0.001 ) && ( newvalue<1000 ) )
					c3 = newvalue;
				break;
			
			case P_C4:
				if( ( newvalue>=0.001 ) && ( newvalue<1000 ) )
					c4 = newvalue;
				break;
			
			default:
				break;
		}
		sizstrvalue = 0;
		strvalue[0] = 0;
		return 0;
	}
	
	if( ( znak >= '0' ) && ( znak <= '9' ) ) {
		if( sizstrvalue < sizeof(strvalue) - 1 ){
			strvalue[sizstrvalue++] = znak;
			strvalue[sizstrvalue] = 0;
		}
		fprintf (stderr, "\r %s", strvalue );
		return 0;
	}
	if( znak == '.' ) {
		if( sizstrvalue < sizeof(strvalue) - 1 ){
			strvalue[sizstrvalue++] = znak;
			strvalue[sizstrvalue] = 0;
		}
		fprintf (stderr, "\r %s", strvalue );
		return 0;
	}
	if( ( znak == 8 ) || ( znak == 127 )) {
		fprintf (stderr, "\r              \r" );
		if(sizstrvalue) sizstrvalue -= 1;
		strvalue[sizstrvalue] = 0;
		fprintf (stderr, "\r %s", strvalue );
		return 0;
	}

	sizstrvalue = 0;
	strvalue[0] = 0;	
	fprintf (stderr, "\r                                                 \r" );
	
  /* ESC */
	poselem[point][2] = 0;
	
  if( ( znak == 27 ) && czytajbezczasu(sekesc) ) {
		if ( !strncmp( sekesc, KEY_DOWN_STR, 3 ) ) {
			changevalue = -1;
			znesc = -1;
		}
		if ( !strncmp( sekesc, KEY_UP_STR , 3 ) ){
			changevalue = 1;
			znesc = 1;
		}
		if ( strncmp( sekesc, KEY_LEFT_STR, 3 ) == 0 ) {
			if(point) --point;
		}
		if ( strncmp( sekesc, KEY_RIGHT_STR, 3 ) == 0 ) {
			if(point<P_END-1) ++point;
		}
	}
	
	poselem[point][2] = 1;
	
	if ( znak == '+' ) changevalue = 0.1;
	if ( znak == '-' ) changevalue = -0.1;
	
	switch(point){
		
		case P_FILTR:
			typefilter += znesc;
			if( typefilter == SIZ_STR_FLTR ) typefilter = SIZ_STR_FLTR-1;
			if( typefilter > (SIZ_STR_FLTR-1) ) typefilter = 0;
			break;
		
		case P_T:
			wspt += changevalue;
			if( ( wspt<0.1 ) || (wspt>10000 ) ) wspt -= changevalue;
			break;
		
		case P_I:
			wsp += changevalue;
			if( ( wsp<0.1 ) || (wsp>10000 ) ) wsp -= changevalue;
			break;
		
		case P_IFL:
			wspfl += changevalue;
			if( ( wspfl<0.1 ) || (wspfl>10000 ) ) wspfl -= changevalue;
			break;
		
		case P_MULTPWM:
			if( znesc<0 && ( multiplepwm > 1 ) ) multiplepwm >>= 1;
			if( znesc>0 && ( multiplepwm < 4096 ) ) multiplepwm <<= 1;
			setpwmrel();
			break;
			
		case P_TIMES:
			if( (znesc<0) && ( maxtimsym > 1000 ) ) maxtimsym -= 1000;
			if( (znesc>0) && ( maxtimsym < 1000000 ) ) maxtimsym += 1000;
			break;
		
		case P_CKLPWM:
			if( znesc<0 && ( _CLKGLOB > 1 ) ) _CLKGLOB -= 1;
			if( znesc>0 && ( _CLKGLOB < 10000 ) ) _CLKGLOB += 1;
			setpwmrel();
			break;
		
		case P_MANHA:
			if(znesc){
				if(manhtm) manhtm = 0;
				else manhtm = 1;
			}
			break;
			
		case P_MANUA:
			if(znesc){
				if(manuall) manuall = 0;
				else manuall = 1;
			}
			break;

		case P_R1:
			r1 += changevalue;
			if( ( r1<0.1 ) || ( r1>1000000 ) ) r1 -= changevalue;
			break;
		
		case P_R2:
			r2 += changevalue;
			if( ( r2<0.1 ) || ( r2>1000000 ) ) r2 -= changevalue;
			break;
		
		case P_R3:
			r3 += changevalue;
			if( ( r3<0.1 ) || ( r3>1000000 ) ) r3 -= changevalue;
			break;

		case P_R4:
			r4 += changevalue;
			if( ( r4<0.1 ) || ( r4>1000000 ) ) r4 -= changevalue;
			break;
		
		case P_C1:
			c1 += changevalue;
			if( ( c1<0.001 ) || ( c1>1000 ) ) c1 -= changevalue;
			break;
		
		case P_C2:
			c2 += changevalue;
			if( ( c2<0.001 ) || ( c2>1000 ) ) c2 -= changevalue;
			break;
		
		case P_C3:
			c3 += changevalue;
			if( ( c3<0.001 ) || ( c3>1000 ) ) c3 -= changevalue;
			break;
		
		case P_C4:
			c4 += changevalue;
			if( ( c4<0.001 ) || ( c4>1000 ) ) c4 -= changevalue;
			break;
	
		
		default:
			break;
	}
		
	//fprintf (stderr, " zn: %u  ", znak );
	
	if( ( znak == 'a' ) && writedatahtm ){
		fprintf (stderr, "\r                                        " );
		fprintf (stderr, "\r write to file" );
		if(!plikhtm) plikhtm = starthtml( namehtm );
		savetohtml( plikhtm );
		writedatahtm = 0;
	}
  if ( znak == 'c' ) { plikhtm = endhtml( plikhtm ); }
  if ( znak == 'j' ) rozj ^= 1;
  if ( znak == 'i' ) invers ^= 1;
  if ( znak == 'q' ) qwit(0);
  if ( znak == 'Q' ) qwit(0);
  if ( znak == 'r' ) {
	  uint32_t timms;
	  fprintf (stderr, "\r                                        " );
	  fprintf (stderr, "\r start analize filter..." );
	  runptl();
	  timms = gettimems();
	  writedatahtm = 1;
	  fprintf (stderr, " done %5u ms   ", timms );
	  }
  if ( znak == 'f' ) if ( ++kolor > 37) kolor = 30;
  if ( znak == 'b' ) if ( ++bkolor > 49) bkolor = 40;
 
	if( point == 0 ){
		if( ( znak >= '0' ) && ( znak <= '9' ) ) typefilter = znak - '1';
	}
 
#if sekenceinne==1
    if ( strncmp( sekesc, "[2~\0", 3 ) == 0 ) /* insert   */
    if ( strncmp( sekesc, "[5~\0", 3 ) == 0 ) /* pg up   */
    if ( strncmp( sekesc, "[6~\0", 3 ) == 0 ) /* pd down */
    if ( strncmp( sekesc, "[Z\0", 3 ) == 0 )  /* alt+tab */
    if ( strncmp( sekesc, "\t\0", 3 ) == 0 )  /* alt+tab inne mapowanie */
    if ( strncmp( sekesc, "[A\0", 3 ) == 0 )  /*   up    */
    if ( strncmp( sekesc, "[B\0", 3 ) == 0 )  /*   down  */
    if ( strncmp( sekesc, "[C\0", 3 ) == 0 )  /*   right */
    if ( strncmp( sekesc, "[D\0", 3 ) == 0 )  /*   lewt  */
#endif
  
  return znak;
}

/* ------------------------------------------------- */

void saveelem1( FILE*plik, double v, uint8_t typ ){
	char buf[16];
	
	fprintf( plik, "%s", namelem[typ] );
	if( v > 999999999 ) sprintf( buf, "  big  " );
	else snprintf(buf, 12, "%9.5f", v );
	buf[10] = 0;
	fprintf( plik, "%s ", buf );
}

/* ------------------------------------------------- */
/* if automatic: name T I IFL */ 
/* if manual name number */

void gnuplotaddnames_tim( char * path, char *name );
void gnuplotaddnames_pk( char * path, char *name );
void gnuplotaddnames_pkt( char * path, char *name );
	
FILE * filedatanameopen( char * what ){

	FILE * f;
	char * pathname;
	char * p, * pr;
	
	pathname = malloc(2048);
	if( pathname == NULL ) return NULL;
	if( what == NULL ) return NULL;

	/* mkdir dir path nmcal */
	snprintf( pathname, 1020, "%s/d%03u/%02u_%s", basepath, nmbplikhtm, nmbcalc, what );
	mkdir( pathname, 0755 );
	
	p = pathname;
	
	if(manuall){
		p += sprintf( p, "%s/d%03u/", basepath, nmbplikhtm );
		p += sprintf( p, "%02u_%s/", nmbcalc, what );
		pr = p;
		p += sprintf( p, "%s", fltrfile[typefilter] );
		p += sprintf( p, "-%02u", nmbcalc );
	} else {
		p += sprintf( p, "%s/d%03u/", basepath, nmbplikhtm );
		p += sprintf( p, "%02u_%s/", nmbcalc, what );
		pr = p;
		p += sprintf( p, "%s", fltrfile[typefilter] );
		p += sprintf( p, "-%02u", nmbcalc );
		p += sprintf( p, "-%3.1f", wspt );
		p += sprintf( p, "-%4.2f", wsp );
		p += sprintf( p, "-%4.2f", wspfl );
		p += sprintf( p, "-%u", multiplepwm );
	}
	
	/* add to gnuplot */
	if( !strcmp(what,"tim") ) gnuplotaddnames_tim( pathname, pr );
	if( !strcmp(what,"pkpk") ) gnuplotaddnames_pk( pathname, pr );
	if( !strcmp(what,"pktotim") ) gnuplotaddnames_pkt( pathname, pr );

	f = fopen( pathname, "w" );
	free(pathname);
	return f;
}

/* ------------------------------------------------- */

void gnuplotstartterm( char * termout );
void gnuplotend();
void gnuplotaddlines( void );

void filenuplotnameopen( void ){
	
	snprintf( pathfilter, 380, "%s/d%03u/g_tim", basepath, nmbplikhtm  );
	snprintf( pathpkpk, 380, "%s/d%03u/g_pkpk", basepath, nmbplikhtm  );
	snprintf( pathpktotim, 380, "%s/d%03u/g_timpk", basepath, nmbplikhtm  );

	if( gnuplotterm & (1<<G_PNG) ){
		gnuplotstartterm( "png" );
		return;
	}
	if( gnuplotterm & (1<<G_JPEG) ) {
		gnuplotstartterm( "jpeg" );
		return;
	}
	if( gnuplotterm & (1<<G_PNGCAIRO) ) {
		gnuplotstartterm( "pngcairo" );
		return;
	}
	
	gnuplotpicext[0] = 0;
	gnuplotstartterm( "x11" );
}

/* ------------------------------------------------- */

int gnuplotmkpicture( void ){
	
	char * command;
	int ret;
	
	command = malloc(4096);	
	if( NULL == command ) return 1;
	if( gnuplotterm == G_NULL ) return 1;
	
	ret = 0;
	
	sprintf(
	command,
	"gnuplot %s 2>/dev/null > %s/d_%03u-tim.%s",
	pathfilter, basepath, nmbplikhtm, gnuplotpicext
	);
	ret |= system( command );

	sprintf(
	command,
	"gnuplot %s 2>/dev/null > %s/d_%03u-pk.%s",
	pathpkpk, basepath, nmbplikhtm, gnuplotpicext
	);
	ret |= system( command );
	
	sprintf(
	command,
	"gnuplot %s 2>/dev/null > %s/d_%03u-tpk.%s",
	pathpktotim, basepath, nmbplikhtm, gnuplotpicext
	);
	ret |= system( command );
	
	free( command );
	return ret;
}

/* ------------------------------------------------- */

void savetohtml( FILE * plik ) {

	FILE * f;
	
	if( !plik ) return;
	
	fprintf( plik, "<pre>" );
	fprintf( plik, "NMB %u\n", nmbcalc );
	fprintf( plik, "FLTR %s\n", fltr[typefilter] );
	fprintf( plik, "TIMES %u s\n", maxtimsym/1000 );
	fprintf( plik, "CPWM %u\n", _CLKGLOB );
	fprintf( plik, "\n" );
	fprintf( plik, "MPWM %u\n", multiplepwm );
	fprintf( plik, "\n" );
	
	saveelem1( plik, r1, P_R1 );
	saveelem1( plik, c1, P_C1 );
	fprintf( plik, "\n" );
	
	saveelem1( plik, r2, P_R2 );
	saveelem1( plik, c2, P_C2 );
	fprintf( plik, "\n" );
	
	saveelem1( plik, r3, P_R3 );
	saveelem1( plik, c3, P_C3 );
	fprintf( plik, "\n" );
	
	saveelem1( plik, r4, P_R4 );
	saveelem1( plik, c4, P_C4 );
	fprintf( plik, "\n" );
	
	fprintf( plik, "\n" );
	showdanehtm( plik );
	fprintf( plik, "</pre><br>\n" );
	fprintf( plik, "<hr>\n" );

	/* data files to dir */
	f = filedatanameopen( "tim" );
	dtftim( f );
	f = filedatanameopen( "pkpk" );
	dtfpkpk( f );
	f = filedatanameopen( "pktotim" );
	dtfpktotim( f );

	gnuplotaddlines();
	
	++nmbcalc;
}

/* ------------------------------------------------- */

FILE * fileexistsnoopen( const char * filename ) {
  FILE *file;
  
  file = fopen(filename, "r");
  if( file ){
    fclose(file);
    return NULL;
  }
  
  return fopen(filename, "w");
}

/* ------------------------------------------------- */

FILE * starthtml( char *name ) {
	
	FILE * plik;
	char * pathname;
	char proc;
	
	nmbcalc = 0;
	pathname = malloc(2048);
	proc = 37;
	
	while(1){
		snprintf( pathname, 1000, "%s/%s_%03u.html", basepath, name, nmbplikhtm );
		plik = fileexistsnoopen( pathname );
		if(plik) break;
		if( ++nmbplikhtm == 1000 ) return NULL;
	}
	
	fprintf( plik, "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">" );
	fprintf( plik, "<title>pwmfl</title>" );
	fprintf( plik, "</head>" );
	fprintf( plik, "<body>\n" );
	
	/* pictures */
	fprintf( plik, "<a" );
	fprintf( plik, " href=\"./d_%03u-tim.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " target=\"_blank\">\n" );
	fprintf( plik, "<img" );
	fprintf( plik, " src=\"./d_%03u-tim.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " width=32%c", proc );
	fprintf( plik, " alt=\"./d_%03u-tim\">", nmbplikhtm );
	fprintf( plik, "</a>\n" );
	
	/* pictures */
	fprintf( plik, "<a" );
	fprintf( plik, " href=\"./d_%03u-pk.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " target=\"_blank\">\n" );
	fprintf( plik, "<img" );
	fprintf( plik, " src=\"./d_%03u-pk.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " width=32%c", proc );
	fprintf( plik, " alt=\"./d_%03u-pk\">", nmbplikhtm );
	fprintf( plik, "</a>\n" );
	
	/* pictures */
	fprintf( plik, "<a" );
	fprintf( plik, " href=\"./d_%03u-tpk.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " target=\"_blank\">\n" );
	fprintf( plik, "<img" );
	fprintf( plik, " src=\"./d_%03u-tpk.%s\"", nmbplikhtm, gnuplotpicext );
	fprintf( plik, " width=32%c", proc );
	fprintf( plik, " alt=\"./d_%03u-tpk\">", nmbplikhtm );
	fprintf( plik, "</a>\n" );
	
	fprintf( plik, "<hr>" );
	
	/* mkdir dir path nmhtml */
	snprintf( pathname, 1020, "%s/d%03u", basepath, nmbplikhtm );
	mkdir( pathname, 0755 );
	filenuplotnameopen();
	free(pathname);
	
	return plik;
}

/* ------------------------------------------------- */

void * endhtml( FILE * plik ) {
	gnuplotend();
	if( !plik ) return NULL;
	fprintf( plik, "</body></html>" );
	fclose(plik);
	fprintf (stderr, "\r                                        " );
	fprintf (stderr, "\r close html file" );
	
	fprintf (stderr, " %s ", gnuplotpicext );

	/* if has gnuplot add charts */	
	if( gnuplotterm ) gnuplotmkpicture();
	
	return NULL;
}

/* ------------------------------------------------- */

#if GNUPLOT_RUTINE==1

int pipefd1[2];
int pipefd2[2];
pid_t pidek;
uint16_t setkolor;

char *strcomm;
extern int colortbl[16];

void __gnuplotplotstart( void ){
	
	if (pipe(pipefd1) == -1) {
		perror("pipe1");
		return;
	}
	if (pipe(pipefd2) == -1) {
		perror("pipe2");
		return;
	}
	
	setkolor = 0;
	
	/* nowy proces */
	pidek = fork();
	if(pidek<0) return;
	
	  /* jesli nowy proces to startuj z opcjami */
  if ( pidek == 0 ) {
  
	  close(pipefd1[1]); //write fd1
	  close(pipefd2[0]); //read fd2
	  close(0); //r
	  close(1); //w
	  dup2(pipefd2[1],1);
	  dup2(pipefd1[0],0);
	  
		fprintf ( stderr, "start ... \n" );
		execlp ( "gnuplot" , "gnuplot", (char * ) NULL );
	  
		perror ("runtxt chdir");
		close(pipefd1[0]);
		close(pipefd2[1]);
		exit(0);
	}
 
	int i;
	char *p;
	uint32_t res;
	
	close(pipefd1[0]);
	close(pipefd2[1]);
	
	strcomm = malloc(1024);

	
	sprintf( strcomm, "set title'time to settings digits'\n" );
	
	sprintf( strcomm, "set term x11 size 980,720\n" );
	res = write( pipefd1[1], strcomm, strlen(strcomm) );
	sprintf( strcomm, "set key on opaque\n" );
	res = write( pipefd1[1], strcomm, strlen(strcomm) );
	sprintf( strcomm, "set grid lc rgb '#888888'\n" );
	res = write( pipefd1[1], strcomm, strlen(strcomm) );
	
	for( i=0; i<(int)(sizeof(colortbl)/sizeof(colortbl[0])); ++i ){
		p = strcomm;
		p += sprintf( p, "set style line %u", i+1 );
		p += sprintf( p, " lt rgb '#%06x'", colortbl[i] );
		p += sprintf( p, " lw 2 pt 0\n" );
		res = write( pipefd1[1], strcomm, strlen(strcomm) );
	}
	sprintf( strcomm, "plot " );
	res = write( pipefd1[1], strcomm, strlen(strcomm) );
	if( res < 1 ) fprintf( stderr, "bad write" );
		
	//res = write( pipefd1[1], strcomm, strlen(strcomm) );
	//if( res < 1 ) perror ("bad write");
	//close(pipefd1[1]);
	//strread = malloc(101);
	/*
	while(1){
		res = read( pipefd2[0], strread, 100 );
		if ( res < 1 ) break;
		strread[res] = 0;
		fprintf (stderr, "%s", strread );
	}*/
	
//	fprintf (stderr, "%u", pidek );
//	waitpid( pidek, &fret, 0 );
	
	fprintf (stderr, "-------" );
	
//	close(pipefd2[0]);
}
#endif

/* ------------------------------------------------- */

void gnuplotinf( void ){

	int pipefd1[2];
	int pipefd2[2];
	int fret;
	char *pch;
	pid_t pidek;
	
	gnuplotexist = 1;
	
	if (pipe(pipefd1) == -1) {
		perror("pipe1");
		return;
	}
	if (pipe(pipefd2) == -1) {
		perror("pipe2");
		return;
	}
	
	/* nowy proces */
	pidek = fork();
	if(pidek<0) return;
	
	  /* jesli nowy proces to startuj z opcjami */
  if ( pidek == 0 ) {
  
	  close(pipefd1[1]); //write fd1
	  close(pipefd2[0]); //read fd2
	  close(0); //r
	  close(1); //w
	  dup2(pipefd2[1],1);
	  dup2(pipefd1[0],0);

	  execlp ( "gnuplot" , "gnuplot", (char * ) NULL );
	  
		gnuplotexist = 0;
		perror ("runtxt chdir");
		close(pipefd1[0]);
		close(pipefd2[1]);
		//return;
		exit(0);
	}
 
	char strcomm[] = "set terminal\n";
	char *strread, *buf;
	uint32_t res, sumr;
	
	close(pipefd1[0]);
	close(pipefd2[1]);
	
	res = write( pipefd1[1], strcomm, strlen(strcomm) );
	//if( res < 1 ) perror ("bad write");
	
	res = fprintf( stdout, "%s", strcomm );
	close(pipefd1[1]);

	strread = malloc(20000);
	buf = malloc(256);
	strread[0] = 0;
	gnuplotterm = 0;
	pch = strread;
	sumr = 0;
	
	while(1){
		res = read( pipefd2[0], pch, 20000 - sumr );
		if ( res < 1 ) break;
		pch += res;
		sumr += res;
		*pch = 0;
	}
	
	fprintf (stderr, " ,%u, %s", res , strread );
	
	fprintf (stderr, "%u", pidek );
	//waitpid( pidek, &fret, WNOHANG|WUNTRACED|WCONTINUED );
	waitpid( pidek, &fret, 0 );
	
	close(pipefd2[0]);
	
	/* find term */
	pch = strread;

	while( NULL !=	(pch = getnwathever ( buf, pch, 256 ) ) ) {
		fprintf (stderr, ".%s.", buf );
		if( !strcmp( "png", buf) ) gnuplotterm |= 1<<G_PNG;
		if( !strcmp( "jpeg", buf) ) gnuplotterm |= 1<<G_JPEG;
		if( !strcmp( "pngcairo", buf) ) gnuplotterm |= 1<<G_PNGCAIRO;
		for( ; *pch != '\n'; ++pch ) if(*pch==0) break;
	}
	free(strread);
	
	if( gnuplotterm & (1<<G_PNG) ){
		sprintf( gnuplotpicext , "png" );
		return;
	}
	if( gnuplotterm & (1<<G_JPEG) ) {
		sprintf( gnuplotpicext , "jpg" );
		return;
	}
	if( gnuplotterm & (1<<G_PNGCAIRO) ) {
		sprintf( gnuplotpicext , "png" );
		return;
	}
	
	gnuplotpicext[0] = 0;
}

/* ---------------------------------------------------------- */
/*         find something and copy                            */
/* ---------------------------------------------------------- */

char * getnwathever (char *gdzie, void *c, int max ) {
  uint16_t ile = 0;
  unsigned char *co = c;
  
  if( co==NULL ) return NULL;
  if( max<2 ) return NULL;
  
  while(1) {
    if( *co==0 ) return NULL;
    if( *co>0x20 ) break;
    ++co;
  }
  
  while(1) {
    *gdzie = *co;
    if( *co<33 ) break;
    if( ++ile == max ) {
      *gdzie = 0;
      return NULL;
    };
    ++gdzie;
    ++co;
  }

  *gdzie = 0;
  
  return (char*) co;
}

/* ------------------------------------------------- */

