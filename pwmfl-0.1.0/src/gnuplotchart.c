/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*-------------------------------------------*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "ha.h"

/*-------------------------------------------*/

extern char basepath[128];
extern char namefilter[64];
extern char pathfilter[192];
extern char pathpkpk[192];
extern char pathpktotim[192];

#define steptlsiz 9
extern uint32_t steptime[steptlsiz];
extern double pkpk[steptlsiz];
extern double pktotim[steptlsiz];
extern uint16_t stepnmb;

extern uint16_t maxdecym;

/*-------------------------------------------*/

void dtftim( FILE * f ) {
	uint16_t i;
	if( f == NULL ) return;
	for( i=0; i<steptlsiz; ++i ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %3u", steptime[i] );
		fprintf( f, "\n" );
	}
	fclose(f);
}

void dtfpkpk( FILE * f ) {
	uint16_t i;
	if( f == NULL ) return;
	for( i=0; i<steptlsiz; ++i ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %f", pkpk[i]*100 );
		fprintf( f, "\n" );
	}
	fclose(f);
}

void dtfpktotim( FILE * f ) {
	uint16_t i;
	if( f == NULL ) return;
	for( i=0; i<steptlsiz; ++i ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %f", pktotim[i] );
		fprintf( f, "\n" );
	}
	fclose(f);
}

/*-------------------------------------------*/

void danetimtofile(void){
	FILE * f;
	double uz;
	uint16_t i, d;
	char pn[256];
	
	sprintf( pn, "%s/%s", pathfilter, namefilter );
	
	f = fopen( pn, "w" );	
	if( NULL == f ) {
		fprintf( stderr, "bad open %s\n", pn );
		return;
	}
	
	if( steptlsiz < maxdecym ) d = steptlsiz;
	else d = maxdecym;
	
	for( uz=0.1, i=0; i<d; ++i, uz/=10 ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %3u", steptime[i] );
		fprintf( f, "\n" );
	}
	
	fclose(f);
}

void danepkpktofile( void ){
	FILE * f;
	uint16_t i, d;
	char pn[256];
	
	sprintf( pn, "%s/%s", pathpkpk, namefilter );
	
	f = fopen( pn, "w" );	
	if( NULL == f ) {
		fprintf( stderr, "bad open %s\n", pn );
		return;
	}
	
	if( steptlsiz < maxdecym ) d = steptlsiz;
	else d = maxdecym;
	
	for( i=0; i<d; ++i ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %f", pkpk[i]*100 );
		fprintf( f, "\n" );
	}
	
	fclose(f);
}

void danepktttofile( void ){
	FILE * f;
	uint16_t i, d;
	char pn[256];
	
	sprintf( pn, "%s/%s", pathpktotim, namefilter );
	
	f = fopen( pn, "w" );
	if( NULL == f ) {
		fprintf( stderr, "bad open %s\n", pn );
		return;
	}
	
	if( steptlsiz < maxdecym ) d = steptlsiz;
	else d = maxdecym;
	
	for( i=0; i<d; ++i ) {
		if( 0 == steptime[i] ) break;
		fprintf( f, "%3u", i+1 );
		fprintf( f, " %f", pktotim[i] );
		fprintf( f, "\n" );
	}
	
	fclose(f);
}

/*-------------------------------------------*/

FILE *gnuplot = NULL;
FILE *gnuplotpk = NULL;
FILE *gnuplotpkt = NULL;
int8_t nmblines = 0;

int colortbl[16];

void mkcolor( void ){
	colortbl[0] = 0x000088;
	colortbl[1] = 0x0000bb;
	colortbl[2] = 0x0000ff;
	colortbl[3] = 0x880000;
	colortbl[4] = 0xbb0000;
	colortbl[5] = 0xff0000;
	colortbl[6] = 0x008800;
	colortbl[7] = 0x00bb00;
	colortbl[8] = 0x00ff00;
	colortbl[9] = 0x880088;
	colortbl[10] = 0xbb00bb;
	colortbl[11] = 0xff00ff;
	colortbl[12] = 0x888800;
	colortbl[13] = 0xbbbb00;
	colortbl[14] = 0xffff00;
	colortbl[15] = 0x555555;
}

/*
void mkcolor( void ){
	colortbl[0] = 0x222222;
	colortbl[1] = 0x555555;
	colortbl[2] = 0x888888;
	colortbl[3] = 0xbbbbbb;
	
	colortbl[4] = 0x660000;
	colortbl[5] = 0x990000;
	colortbl[6] = 0xcc0000;
	colortbl[7] = 0xff0000;
	
	colortbl[8] = 0x006600;
	colortbl[9] = 0x009900;
	colortbl[10] = 0x00cc00;
	colortbl[11] = 0x00ff00;
	
	colortbl[12] = 0x0000cc;
	colortbl[13] = 0x0000ff;
	colortbl[14] = 0x000000;
	colortbl[15] = 0x000000;
}
*/
/*
void mkcolor( void ){
	colortbl[9] = 0x444444;
	colortbl[10] = 0x777777;
	colortbl[11] = 0xbbbbbb;
	colortbl[3] = 0x770000;
	colortbl[4] = 0xaa0000;
	colortbl[5] = 0xff0000;
	colortbl[6] = 0x007700;
	colortbl[7] = 0x00aa00;
	colortbl[8] = 0x00ff00;
	colortbl[0] = 0x007777;
	colortbl[1] = 0x00aaaa;
	colortbl[2] = 0x00ffff;
	colortbl[12] = 0x0000cc;
	colortbl[13] = 0x0000ff;
	colortbl[14] = 0x000000;
	colortbl[15] = 0x000000;
}
*/
/*-------------------------------------------*/

void gnuplotinit( FILE * gnuplot ){
	int i;

	/* gif jpeg svg png */
	if(gnuplot){
		//fprintf(gnuplot, "set term wxt size 980,720\n" );
		fprintf(gnuplot, "set key on opaque\n" );
		//fprintf(gnuplot, "set term xlib size 980,720\n" );
		//fprintf(gnuplot, "set term xterm size 980,720\n" );
		//fprintf(gnuplot, "set term pngcairo size 980,760\n" );
		//fprintf(gnuplot, "set term png size 980,760\n" );
		fprintf(gnuplot, "set grid " );
		fprintf(gnuplot, "lc rgb '#888888'\n" );
		/*
		set logscale y 10
		set grid lc rgb '#aaaaaa'
		set term wxt size 980,760 background rgb '#000000'
		set border back lc rgb '#0xaaaaaa'
		set key left textcolor rgb '#0xaaaaaa'
		*/
	
		/*
		fprintf(gnuplot, "set style line 1 lt rgb \"green\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 2 lt rgb \"red\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 3 lt rgb \"blue\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 4 lt rgb \"magenta\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 5 lt rgb \"black\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 6 lt rgb \"orange\" lw 2 pt 0\n" );
		fprintf(gnuplot, "set style line 7 lt rgb \"cyan\" lw 2 pt 0\n" );
		*/
		
		for( i=0; i<(int)(sizeof(colortbl)/sizeof(colortbl[0])); ++i ){
			fprintf(gnuplot, "set style line %u", i+1 );
			fprintf(gnuplot, " lt rgb '#%06x'", colortbl[i] );
			fprintf(gnuplot, " lw 2 pt 0\n" );
		}
		fprintf(gnuplot, "plot " );
	}
}

static void gnuplotentry( char * );

void gnuplotfopen( void ){
	char *pn;
	pn = malloc(1024);
	if(pn==NULL) return;
	sprintf( pn, "%s/g.txt", pathfilter );
	gnuplot = fopen( pn, "w" );
	sprintf( pn, "%s/g.txt", pathpkpk );
	gnuplotpk = fopen( pn, "w" );
	sprintf( pn, "%s/g.txt", pathpktotim );
	gnuplotpkt = fopen( pn, "w" );
	free(pn);
}

static void gnuplotentry( char * termout ) {
	if(gnuplot==NULL) return;
	if(gnuplotpk==NULL) return;
	if(gnuplotpkt==NULL) return;

	fprintf(gnuplot, "set term %s size 980,620\n", termout );
	fprintf(gnuplotpk, "set term %s size 980,620\n", termout );
	fprintf(gnuplotpkt, "set term %s size 980,620\n", termout );
	
	fprintf(gnuplotpk, "set logscale y\n" );
	//fprintf(gnuplotpkt, "set logscale y\n" );
	
	fprintf(gnuplot, "set key left\n" );
	
	fprintf(gnuplot, "set title " );
	fprintf(gnuplotpk, "set title " );
	fprintf(gnuplotpkt, "set title " );
	
	fprintf(gnuplot, "\'%s\'\n", "time to settings digits" );
	fprintf(gnuplotpk, "\'%s\'\n", "pk-pk scale 100%" );
	//fprintf(gnuplotpkt, "\'%s\'\n", "quality compare to RC 1T" );
	fprintf(gnuplotpkt, "\'%s\'\n", "performance compare to RC 1T" );
	
	gnuplotinit(gnuplot);
	gnuplotinit(gnuplotpk);
	gnuplotinit(gnuplotpkt);	
}
	
void gnuplotstart( void ){
	nmblines = 0;
	gnuplotfopen();
	gnuplotentry("x11");
}

void gnuplotstartterm( char * termout ){
	nmblines = 0;
	gnuplot = fopen( pathfilter, "w" );
	gnuplotpk = fopen( pathpkpk, "w" );
	gnuplotpkt = fopen( pathpktotim, "w" );
	gnuplotentry(termout);
}

void gnuplotaddplot( void ){
	if(gnuplot==NULL) return;
	if(gnuplotpk==NULL) return;
	if(gnuplotpkt==NULL) return;
	fprintf(gnuplot, " \"%s\"", namefilter );
	fprintf(gnuplot, " with linespoints ls %u," , 1 + nmblines );
	fprintf(gnuplotpk, " \"%s\"", namefilter );
	fprintf(gnuplotpk, " with linespoints ls %u," , 1 + nmblines );
	fprintf(gnuplotpkt, " \"%s\"", namefilter );
	fprintf(gnuplotpkt, " with linespoints ls %u," , 1 + nmblines );
	++nmblines;
}

void gnuplotaddnames_tim( char * path, char *name ){
	fprintf( gnuplot, " \"" );
	fprintf( gnuplot, "%s", path );
	fprintf( gnuplot, "\"" );
	fprintf( gnuplot, " title" );
	fprintf( gnuplot, " \"" );
	fprintf( gnuplot, "%s", name );
	fprintf( gnuplot, "\"" );
}

void gnuplotaddnames_pk( char * path, char *name ){
	fprintf( gnuplotpk, " \"" );
	fprintf( gnuplotpk, "%s", path );
	fprintf( gnuplotpk, "\"" );
	fprintf( gnuplotpk, " title" );
	fprintf( gnuplotpk, " \"" );
	fprintf( gnuplotpk, "%s", name );
	fprintf( gnuplotpk, "\"" );
}

void gnuplotaddnames_pkt( char * path, char *name ){
	fprintf( gnuplotpkt, " \"" );
	fprintf( gnuplotpkt, "%s", path );
	fprintf( gnuplotpkt, "\"" );
	fprintf( gnuplotpkt, " title" );
	fprintf( gnuplotpkt, " \"" );
	fprintf( gnuplotpkt, "%s", name );
	fprintf( gnuplotpkt, "\"" );
}

void gnuplotaddlines( void ){
	if(gnuplot==NULL) return;
	if(gnuplotpk==NULL) return;
	if(gnuplotpkt==NULL) return;
	fprintf(gnuplot, " with linespoints ls %u," , 1 + nmblines );
	fprintf(gnuplotpk, " with linespoints ls %u," , 1 + nmblines );
	fprintf(gnuplotpkt, " with linespoints ls %u," , 1 + nmblines );
	++nmblines;
}

void gnuplotend( void ){
	if(gnuplot==NULL) return;
	if(gnuplotpk==NULL) return;
	if(gnuplotpkt==NULL) return;
	fprintf(gnuplot, "\n" );
	fprintf(gnuplot, "refresh\n" );
	//fprintf(gnuplot, "pause -1\n" );
	fprintf(gnuplotpk, "\n" );
	fprintf(gnuplotpk, "refresh\n" );
	//fprintf(gnuplotpk, "pause -1\n" );
	fprintf(gnuplotpkt, "\n" );
	//fprintf(gnuplotpkt, "set key left\n" );
	fprintf(gnuplotpkt, "refresh\n" );
	//fprintf(gnuplotpkt, "pause -1\n" );
	fclose(gnuplot);
	fclose(gnuplotpk);
	fclose(gnuplotpkt);
	gnuplot = NULL;
	gnuplotpk = NULL;
	gnuplotpkt = NULL;
}

/*-------------------------------------------*/

int mkdirworkdir(void){
	char *cmd;
	int i;
	
	cmd = malloc(1024);
	if(cmd==NULL) return 1;
	
	sprintf( cmd, "mkdir %s", basepath );
	i = system( cmd );
	/*
	sprintf( cmd, "mkdir %s", pathfilter );
	i = system( cmd );
	sprintf( cmd, "mkdir %s", pathpkpk );
	i = system( cmd );
	sprintf( cmd, "mkdir %s", pathpktotim );
	i = system( cmd );
	*/
	free(cmd);
	return i;
}

/*-------------------------------------------*/

int mkdirworkdir_t( uint16_t n ){
	char *cmd;
	int i;

	cmd = malloc(1024);
	if(cmd==NULL) return 1;
	
	sprintf( cmd, "mkdir %s/%05u", basepath, n );
	i = system( cmd );
	
	sprintf( pathfilter, "%s/%05u/%s", basepath, n, "tim" );
	sprintf( pathpkpk, "%s/%05u/%s", basepath, n, "pkpk" );
	sprintf( pathpktotim, "%s/%05u/%s", basepath, n, "pktim" );
	
	sprintf( cmd, "mkdir %s", pathfilter );
	i = system( cmd );
	sprintf( cmd, "mkdir %s", pathpkpk );
	i = system( cmd );
	sprintf( cmd, "mkdir %s", pathpktotim );
	i = system( cmd );
	free(cmd);
	return i;	
}

/*-------------------------------------------*/

void gnuplotmake( void ) {
	danetimtofile();
	danepkpktofile();
	danepktttofile();
	gnuplotaddplot();
}

/*-------------------------------------------*/
