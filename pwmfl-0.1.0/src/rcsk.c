/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
/*-------------------------------------------*/

extern double uc1, r1, c1, il1, usr1, umc1, unc1;
extern double uc2, r2, c2, il2, usr2, umc2, unc2;
extern double uc3, r3, c3, il3, usr3, umc3, unc3;
extern double uc4, r4, c4, il4, usr4, umc4, unc4;
extern double rws, rwg;
extern double uao, uaos, uaom, uaon;
extern double *tblu;
extern double t, tb, uz;

/*-------------------------------------------*/

void nextselkey( void ) {

	il2 = (uc1+uao-uc2)/r2;
	il1 = (uz-uao-uc1)/r1 - il2;
	
	uc1 += t*il1/c1;
	uc2 += t*il2/c2;
	
	if( rws == 0 ) uao = uc2;
	else uao = uc2 * (1 + rws/rwg);
}

/*-------------------------------------------*/
/* without amplify */

void next2xsk( void ) {

	il2 = uc1/r2;
	il1 = (uz-uc2-uc1)/r1 - il2;
	
	il4 = uc3/r4;
	il3 = (uc2-uc4-uc3)/r3 - il4;
	
	uc1 += t*il1/c1;
	uc2 += t*il2/c2;
	uc3 += t*il3/c3;
	uc4 += t*il4/c4;

	uao = uc4;
}

/*-------------------------------------------*/

void nextrcsk( void ) {

	il3 = (uc2+uao-uc3)/r3;
	il2 = (uc1-uao-uc2)/r2 - il3;
	il1 = (uz-uc1)/r1 + (uao+uc2-uc1)/r2;
	
	uc1 += t*il1/c1;
	uc2 += t*il2/c2;
	uc3 += t*il3/c3;
	
	if( rws == 0 ) uao = uc3;
	else uao = uc3 * (1 + rws/rwg);
	
	if ( uaom < uao ) uaom = uao;
	if ( uaon > uao ) uaon = uao;
}
	
/*-------------------------------------------*/

void nextsk4( void ) {

	il4 = (uc3+uao-uc4)/r4;
	il3 = (uc2-uao-uc3)/r3 - il4;
	il2 = (uc1+uao-uc2)/r2 + (uc3+uao-uc2)/r3;
	il1 = (uz-uc1-uao)/r1 - (uao+uc1-uc2)/r2;
	
	uc1 += t*il1/c1;
	uc2 += t*il2/c2;
	uc3 += t*il3/c3;
	uc4 += t*il4/c4;
	
	if( rws == 0 ) uao = uc4;
	else uao = uc4 * (1 + rws/rwg);
	
	if ( uaom < uao ) uaom = uao;
	if ( uaon > uao ) uaon = uao;
}
	
/*-------------------------------------------*/
