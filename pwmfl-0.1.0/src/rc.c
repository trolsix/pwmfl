/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
extern double uc1, r1, c1, il1, usr1, umc1, unc1;
extern double uc2, r2, c2, il2, usr2, umc2, unc2;
extern double uc3, r3, c3, il3, usr3, umc3, unc3;
extern double rws, rwg;
extern double uao, uaos, uaom, uaon;
extern double *tblu;
extern double t, tb, uz;


void nextrc1( void ) {
	il1 = ( uz - uc1 )/r1;
	uc1 += (t*il1)/c1;
	uao = uc1;
}

void nextrc2( void ) {
	
	il1 = ( uz - uc1 )/r1 + ( uc2 - uc1 )/r2;
	il2 = ( uc1 - uc2 )/r2;
	uc1 += (t*il1)/c1;
	uc2 += (t*il2)/c2;
	uao = uc2;
}

void nextrc3( void ) {
	
	il1 = ( uz - uc1 )/r1 + ( uc2 - uc1 )/r2;
	il2 = ( uc1 - uc2 )/r2 + ( uc3 - uc2 )/r3;
	il3 = ( uc2 - uc3 )/r3;
	
	uc1 += t*il1/c1;
	uc2 += t*il2/c2;
	uc3 += t*il3/c3;
	uao = uc3;
}

void nextrc3buf( void ) {
	
	il1 = ( uz - uc1 )/r1;
	il2 = ( uc1 - uc2 )/r2;
	il3 = ( uc2 - uc3 )/r3;
	
	uc1 += (t*il1)/c1;
	uc2 += (t*il2)/c2;
	uc3 += (t*il3)/c3;
	uao = uc3;
}

