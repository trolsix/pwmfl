/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
/*-------------------------------------------*/

/*#include <inttypes.h> */
#include <stdlib.h> 
#include <stdint.h> 
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>

#include <math.h>

#include "ha.h"
#include "timeutil.h"

/*-------------------------------------------*/

char version[] = "0.1.0";

/*-------------------------------------------*/

void testterm(void);
int wczytajstr ( char * a, char * b, size_t s );

/*-------------------------------------------*/

#define LLU long long unsigned

/*-------------------------------------------*/

void minmax( void );
void minmaxclr( void );

void test1( void );
void test2( void );
void test3( void );
void test4( void );
void test5( void );
void test6( void );
void test7( void );
void test8( void );
void test9( void );

void (*funfun)( void );

void (*tblfun[])= {
	nextrc1, nextrc2, nextrc3, nextrc3buf, nextselkey, nextrcsk, nextsk4, next2xsk
};

/*-------------------------------------------*/

double uc1, r1, c1, il1, usr1, umc1, unc1;
double uc2, r2, c2, il2, usr2, umc2, unc2;
double uc3, r3, c3, il3, usr3, umc3, unc3;
double uc4, r4, c4, il4, usr4, umc4, unc4;
double rws, rwg;
double uao, uaos, uaom, uaon;
double t, tb, uz;
double errormax, wsp, wspt, wspcc, wspi, period;

int64_t ptl;
int16_t maxdecym;
uint32_t tnmb;
uint32_t _CLKGLOB;
double pwmd;

uint16_t nrtest;
uint8_t rcqualitydone;

uint16_t multiplepwm;

void (*wskfun)(void);

uint32_t maxtimsym;

/*-------------------------------------------

     pwm

-------------------------------------------*/

#define  REALBITPWM  10

uint32_t BITPWM;
uint32_t uppwm;
uint32_t realuppwm;
uint32_t mainpwm;
uint32_t realpwm;

static uint32_t mainclk;
static uint8_t tickpwm;
static uint32_t bpwm;
static uint32_t realbpwm;

uint8_t getpwm( void ) {
	if( 0 == mainclk ) {
		++bpwm;
		if( ++realbpwm > realuppwm ) realbpwm = 0;
		if( bpwm == uppwm+1 ) tickpwm = 1;
		if( bpwm > uppwm ) {
			bpwm = 0;
			realbpwm = 0;
		}
	}

	if( realbpwm <= realpwm ) return 1;
	return 0;
}

void zerpwm( void ) {
	tickpwm = 0;
	bpwm = 0;
	realbpwm = 0;
	realpwm = mainpwm/multiplepwm;
	realuppwm = uppwm/multiplepwm;
}

/*-------------------------------------------*/

void setpwmrel( void ){
	/*
	if(  ((1+uppwm)/multiplepwm) < 64 ) {
		uppwm = (multiplepwm*64)-1;
	}
	if(  ((1+uppwm)/multiplepwm) > 64 ) {
		uppwm = (multiplepwm*64)-1;
	}*/
	mainclk = _CLKGLOB;
	uppwm = (multiplepwm*64)-1;
	//mainpwm = uppwm;
	mainpwm = uppwm>>1;
	ptl = (uppwm+1)*_CLKGLOB;
	t = 1.0 / (double)ptl;
	pwmd = (double)(1+mainpwm) / (double)(1+uppwm);
}


/*-------------------------------------------



-------------------------------------------*/

static double stepvalue = 0.1;
uint16_t stepovers;

#define steptlsiz 9
uint32_t steptime[steptlsiz];
double pkpk[steptlsiz];
double pktotim[steptlsiz];

double pktotimrc[steptlsiz] = {
	0.77401224487,
	1.11270316906,
	1.31029945402,
	1.48477536188,
	1.71435933325,
	1.85172463569,
	1.97957761486,
	2.09965923518,
	2.26789119277
#if steptlsiz > 9
	,2.37343031546,
#endif
#if steptlsiz > 10
	,2.47447215778
#endif
#if steptlsiz > 11
	,2.61873518227
#endif
#if steptlsiz > 12
	,2.71064843749
#endif
};

#define sosiz steptlsiz
uint16_t overshootckl[sosiz];
double overshoot[sosiz];
double overpkpk[sosiz];
uint16_t stepnmb = 0;

void rcqaulityinit(void){
	int i;
	for( i=0; i<steptlsiz; ++i ) pktotimrc[i] = 1.0;
}

void rcqaulityrecalculate(void){
	int i;
	for( i=0; i<steptlsiz; ++i ) {
		pktotimrc[i] = 1/pktotim[i];
		fprintf( stderr, "_%13.11f\n", pktotimrc[i] );
	}
}

/*-------------------------------------------*/

void rcqaulityconst(void){
/*
	uint8_t is = sizeof(pktotimrc) - 1;
	
	switch(is){
		case 12:
			pktotimrc[is--] = 2.71064843749;
		case 11:
			pktotimrc[is--] = 2.61873518227;
		case 10:
			pktotimrc[is--] = 2.47447215778;
		case 9:
			pktotimrc[is--] = 2.37343031546;
		case 8:
			pktotimrc[is--] = 2.26789119277;
		case 7:
			pktotimrc[is--] = 2.09965923518;
		case 6:
			pktotimrc[is--] = 1.97957761486;
		case 5:
			pktotimrc[is--] = 1.85172463569;
		case 4:
			pktotimrc[is--] = 1.71435933325;
		case 3:
			pktotimrc[is--] = 1.48477536188;
		case 2:
			pktotimrc[is--] = 1.31029945402;
		case 1:
			pktotimrc[is--] = 1.11270316906;
		case 0:
			pktotimrc[is] = 0.77401224487;
			break;
		default:
			break;
	}
	*/
	rcqualitydone = 1;
}

/*-------------------------------------------*/

void danesave( double d ) {
	while(1){
		if( stepnmb >= steptlsiz ) return;
		if( d > stepvalue ) return;
		pkpk[stepnmb] = uaom-uaon;
		pktotim[stepnmb] = pktotimrc[stepnmb] / sqrt ( pkpk[stepnmb] * (double)tnmb );
		steptime[stepnmb++] = tnmb;
		stepvalue /= 10;
	}
}

void danezer(void){
	int i;
	for( i=0; i<steptlsiz; ++i ) {
		steptime[i] = 0;
		pktotim[i] = 0;
		pkpk[i] = 0;
	}
	
	for( i=0; i<sosiz; ++i ) {
		overshoot[i] = 0;
		overshootckl[i] = 0;
		overpkpk[i] = 0;
	}
	stepovers = 0;
	stepvalue = 0.1;
	stepnmb = 0;
}

/*-------------------------------------------*/

char namefilter[64];
char basepath[256] = "/tmp/pwmf";
char pathfilter[384] = "\0";
char pathpkpk[384] = "\0";
char pathpktotim[384] = "\0";

/*-------------------------------------------*/

void showdane(void){
	int32_t i;
	double uz;

	fprintf( stderr, "%s\n", namefilter );
	
	for( uz=0.1 , i=0; i<steptlsiz; ++i, uz/=10 ) {
		if( 0 == steptime[i] ) break;
		fprintf( stdout, "%3u", i+1 );
		fprintf( stdout, " %3u", steptime[i] );
		fprintf( stdout, " %10.7f", pkpk[i] );
		fprintf( stdout, "\n" );
	}
	fprintf( stdout, "\n" );
	fflush( stdout );
}

/*-------------------------------------------*/

void showdanehtm( FILE * filestr ){
	int32_t i;
	double uz;
	
	fprintf( filestr, "aprox tim   pk-pk    overshoot tim   value     pk-pk<br>\n" );
	
	for( uz=0.1 , i=0; i<steptlsiz; ++i, uz/=10 ) {
		fprintf( filestr, "%3u", i+1 );
		fprintf( filestr, " %5u", steptime[i] );
		fprintf( filestr, "  %10.8f", pkpk[i] );
		fprintf( filestr, "      " );
		fprintf( filestr, " %5u", overshootckl[i] );
		fprintf( filestr, " %11.8f", overshoot[i] );
		fprintf( filestr, " %10.8f", overpkpk[i] );
		fprintf( filestr, "           " );
		fprintf( filestr, "\n" );
	}
}	

/*-------------------------------------------*/

void showdanestr( FILE * filestr ){
	int32_t i;
	double uz;
	
	fprintf( filestr, "aprox tim   pk-pk    overshoot tim   value   pk-pk           \n" );

	for( uz=0.1 , i=0; i<steptlsiz; ++i, uz/=10 ) {
		fprintf( filestr, "%3u", i+1 );
		fprintf( filestr, " %5u", steptime[i] );
		fprintf( filestr, "  %10.8f", pkpk[i] );
		fprintf( filestr, "      " );
		fprintf( filestr, " %5u", overshootckl[i] );
		fprintf( filestr, " %11.8f", overshoot[i] );
		fprintf( filestr, " %10.8f", overpkpk[i] );
		fprintf( filestr, "           " );
		fprintf( filestr, "\n" );
	}
//		fprintf(stderr, "                                                             \n" );	
}

/*-------------------------------------------*/

void startval( void ) {
	
	danezer();
	zerpwm();
	minmaxclr();
	uc1  = 0;
	uc3  = uc1;
	uc2  = uc1;
	uc4  = uc1;
	uao  = 0;
	tb   = 0;
	tnmb = 0;
}

/*-------------------------------------------*/

void eleminit( void ) {
	r1  = 1; r2  = 1; r3  = 1; r4  = 1;
	c1  = 1; c2  = 1; c3  = 1; c4  = 1;
}

void elemit( double T ) {
	r1  = 1; r2  = 1; r3  = 1; r4  = 1;
	c1  = T; c2  = T; c3  = T; c4  = T;
}

void elemaw( uint16_t wsp ) {
	r2  *= wsp; r3  *= wsp*wsp; r4  *= wsp*wsp*wsp;
	c2  /= wsp; c3  /= wsp*wsp; c4  /= wsp*wsp*wsp;
}

void elemawf( double wsp ) {
	r2  *= wsp; r3  *= wsp*wsp; r4  *= wsp*wsp*wsp;
	c2  /= wsp; c3  /= wsp*wsp; c4  /= wsp*wsp*wsp;
}

/*-------------------------------------------*/

void minmax( void ) {
	if ( umc1 < uc1 ) umc1 = uc1;
	if ( unc1 > uc1 ) unc1 = uc1;
	if ( umc2 < uc2 ) umc2 = uc2;
	if ( unc2 > uc2 ) unc2 = uc2;
	if ( umc3 < uc3 ) umc3 = uc3;
	if ( unc3 > uc3 ) unc3 = uc3;
	if ( umc4 < uc4 ) umc4 = uc4;
	if ( unc4 > uc4 ) unc4 = uc4;
	if ( uaom < uao ) uaom = uao;
	if ( uaon > uao ) uaon = uao;	
}

void minmaxget( void ) {
	usr1 = usr1 / ptl;
	usr2 = usr2 / ptl;
	usr3 = usr3 / ptl;
	usr4 = usr4 / ptl;
	uaos = uaos / ptl;
}

void minmaxclr( void ) {
	usr1 = 0;
	usr2 = 0;
	usr3 = 0;
	usr4 = 0;
	uaos = 0;
	umc1 = 0;
	umc2 = 0;
	umc3 = 0;
	umc4 = 0;
	uaom = 0;
	unc1 = 1000000;
	unc2 = 1000000;
	unc3 = 1000000;
	unc4 = 1000000;
	uaon = 1000000;
}

void sumsr( void ) {
	usr1 += uc1;
	usr2 += uc2;
	usr3 += uc3;
	usr4 += uc4;
	uaos += uao;
}

/*-------------------------------------------*/

void runptl( void ) {
	
	uint64_t i;
	double valnm1;

	startval();
	valnm1 = 0;
	stepovers = 0;

	starttimer();
	
	//fprintf( stderr, "%f %u %u %u %u %llu", pwmd, realpwm, mainpwm, realuppwm, uppwm, (LLU) ptl );

	for( i=90000*ptl; i; --i ) {
		
		minmax();
		
		/* test time */
		if(  0 == (i & 0xFFFF) ) {
			uint32_t timms;
			timms = gettimems();
			if( timms > maxtimsym ) break;
		}
		
		if( tickpwm ) {
			
			tickpwm = 0;
			++tnmb;
			
			minmaxget();
			
			/* overshoot up and down */
			if( stepovers & 0x01 ) {
				if( uaos > valnm1 ) {
					if( stepovers < sosiz ) {
						overpkpk[stepovers] = uaom-uaon;
						overshoot[stepovers] = uaos;
						overshootckl[stepovers++] = tnmb;
					} else break;
					//fprintf( stderr, "d %3u %11.9f\n", tnmb, uaos );
				}
			} else {
				if( uaos < valnm1 ) {
					if( stepovers < sosiz ) {
						overpkpk[stepovers] = uaom-uaon;
						overshoot[stepovers] = uaos;
						overshootckl[stepovers++] = tnmb;
					} else break;
					//fprintf( stderr, "u %3u %11.9f\n", tnmb, uaos );
				}
			}
			valnm1 = uaos;
			
			danesave((double)(pwmd-uaos) );
		
			if( steptime[steptlsiz-1] ) {
				if( steptime[steptlsiz-1] != steptime[steptlsiz-2] ) break;
			}
			if(stepovers>2){
				double tmp;
				tmp = overshoot[stepovers-1] - overshoot[stepovers-2];
				if(tmp<0) tmp = -tmp;
				if(tmp<0.000000001)  break;
			}
			minmaxclr();
		}
		
		if( mainclk == 0 ) mainclk = _CLKGLOB;
		if( mainclk ) mainclk--;
		
		uz = getpwm();
		
		wskfun();

		sumsr();
		tb += t;
	}

}

/*-------------------------------------------*/

void wpstapply(void){
	r1 *= wspt;
	r2 *= wspt;
	r3 *= wspt;
	r4 *= wspt;
}

/*-------------------------------------------*/

int polecenia( int argc, char *argv[] ) {
  
  int i = 0;
  int u = 0;
  
  while ( ++i < argc ) {
    if ( strcmp ( argv[i], "-h" ) == 0 )  u = 1;
    if ( strcmp ( argv[i], "-H" ) == 0 )  u = 1;
    if ( argv[1][0] == 'h' )  u = 1;
    if ( argv[1][0] == 'H' )  u = 1;

    if (u) {
      fprintf( stderr, "-h  this help\n" );
      fprintf( stderr, "-P  path data\n" );
      return 1;
    }
  }

  for ( i=1; i<argc; ++i ) {
    if ( strcmp ( argv[i], "-P" ) == 0 ) {
      wczytajstr ( basepath, argv[i+1], sizeof(basepath) - 1 );
      ++i;
    }
 }

  return 0;
}


/*-------------------------------------------
      main
-------------------------------------------*/

int main( int argc , char * argv[] ) {
	
	if( polecenia ( argc, argv ) ) return 0;
	
	nrtest = 0;;
	mkdirworkdir();
	mkcolor();
	maxdecym = 10;
	rcqualitydone = 0;
	
	multiplepwm = 1;
	maxtimsym = 10000;

	_CLKGLOB = 32;
	ptl = (uppwm+1)*_CLKGLOB;
	t = 1.0 / (double)ptl;
	
	setpwmrel();
	
	fprintf( stderr, "ptl %llu\n", (LLU) ptl );
	fprintf( stderr, "clkg %u\n", _CLKGLOB );
	fprintf( stderr, "t %25.23f\n", (double)t );
	
	//errormax = 0.0000000000001;
	errormax = 0.0000000001;
	
	uz  = 1;
	rws = 0;
	rwg = 1;
	wsp = 5;
	
	eleminit();
	
	pwmd = (double)(1+mainpwm) / (double)(1+uppwm);

	mainclk = _CLKGLOB;

	wspi = 1;
	wspt = 1;
	wsp = 1;
	
	elemit( wspt );
	elemawf( wsp );
	rcqaulityconst();

	testterm();
	return 0;



/* before before for one type PWM */
	elemit( 1 );
	wskfun = nextrc1;
	rcqaulityinit();
	runptl();
	rcqaulityrecalculate();
	rcqualitydone = 1;
	return 0;
	
	//test1();
	//test2();
	
	test3();
	/*
	test4();
	test5();
	test6();
	test7();
	test8();
	*/
//	uz = exp(-0.5);
//	fprintf( stderr, "%15.13f\n", (double)(1-uz) );

	return 0;
}

/*-------------------------------------------*/

int mkdirworkdir_t( uint16_t n );

void test1( void ) {
	
	int i;
	
	mkdirworkdir_t( nrtest++ );
	eleminit();	
	gnuplotstart();
	
	wskfun = nextrc1;
	
	for( i=1; i<=8; i*=2 ) {
		sprintf( namefilter, "1xRC_%uT", i );
		elemit( i );
		runptl();
		showdane();
		gnuplotmake();
	}
	
	wskfun = nextrc2;

	for( i=1; i<=8; i*=2 ) {
		sprintf( namefilter, "2xRC_%uT", i );
		elemit( i );
		runptl();
		showdane();
		gnuplotmake();
	}

	wskfun = nextrc3;

	for( i=1; i<=8; i*=2 ) {
		sprintf( namefilter, "3xRC_%uT", i );
		elemit( i );
		runptl();
		showdane();
		gnuplotmake();
	}
	
	gnuplotend();	
}

/*-------------------------------------------*/

void test2( void ) {
	
	int i;
	
	mkdirworkdir_t( nrtest++ );
	eleminit();
	wskfun = nextrc3;
	gnuplotstart();
	
	for( wspi=1, i=0; i<4; ++i ) {
		eleminit();
		elemaw(wspi);
		sprintf( namefilter, "3xRC_1T_wsp_%u" , (int)wspi );
		runptl();
		showdane();
		gnuplotmake();
		fprintf( stderr, "%4.1f", wspi );
		fprintf( stderr, " %3.0f %3.0f %3.0f", r1, r2, r3 );
		fprintf( stderr, " %f %f %f\n", c1, c2, c3 );
		wspi *= 2;
	}
	
	for( wspi=1, i=0; i<4; ++i ) {
		elemit(2);
		elemaw(wspi);
		sprintf( namefilter, "3xRC_2T_wsp_%u" , (int)wspi );
		runptl();
		showdane();
		gnuplotmake();
		fprintf( stderr, "%4.1f", wspi );
		fprintf( stderr, " %3.0f %3.0f %3.0f", r1, r2, r3 );
		fprintf( stderr, " %f %f %f\n", c1, c2, c3 );
		wspi *= 2;
	}

	for( wspi=1, i=0; i<4; ++i ) {
		elemit(4);
		elemaw(wspi);
		sprintf( namefilter, "3xRC_4T_wsp_%u" , (int)wspi );
		runptl();
		showdane();
		gnuplotmake();
		fprintf( stderr, "%4.1f", wspi );
		fprintf( stderr, " %3.0f %3.0f %3.0f", r1, r2, r3 );
		fprintf( stderr, " %f %f %f\n", c1, c2, c3 );
		wspi *= 2;
	}
	
	gnuplotend();
}


/*-------------------------------------------*/

void test3( void ) {
	
	int i;
	
	mkdirworkdir_t( nrtest++ );
	eleminit();
	wskfun = nextselkey;
	gnuplotstart();
	
	for( wspi=1, i=1; i<=8; i*=2 ) {
		elemit(i);
		sprintf( namefilter, "sk_%uT", i );
		runptl();
		showdane();
		gnuplotmake();
		wspi *= 2;
	}

	wskfun = nextrc3;
	elemit(1);
	sprintf( namefilter, "3xRC_%uT", 1 );
	runptl();
	showdane();
	gnuplotmake();

	gnuplotend();
}

/*-------------------------------------------*/

extern int colortbl[16];

void test4( void ) {
	
	int i;
	
	mkdirworkdir_t( nrtest++ );
	eleminit();
	wskfun = nextrcsk;
	colortbl[4] = 0xaa00aa;
	
	gnuplotstart();
	
	for( wspi=1, i=1; i<=8; i*=2 ) {
		elemit(i);
		sprintf( namefilter, "rcsk_%uT", i );
		runptl();
		showdane();
		gnuplotmake();
		wspi *= 2;
	}
	
	elemit(1);
	sprintf( namefilter, "rcsk_1T_wsp8" );
	r1 /= 8;
	c1 *= 8;
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextrc3;
	elemit(1);
	sprintf( namefilter, "3xRC_%uT", 1 );
	runptl();
	showdane();
	gnuplotmake();

	gnuplotend();
}

/*-------------------------------------------*/

void test5( void ) {
	
	int i;
	
	mkdirworkdir_t( nrtest++ );
	eleminit();
	wskfun = nextsk4;
	colortbl[4] = 0xaa00aa;
	
	gnuplotstart();
	
	for( i=1; i<=8; i*=2 ) {
		elemit(i);
		sprintf( namefilter, "LP_4th_%uT", i );
		runptl();
		showdane();
		gnuplotmake();
	}
	
	elemit(0.7);
	sprintf( namefilter, "LP_4th_0T5" );
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextrc3;
	elemit(1);
	sprintf( namefilter, "3xRC_%uT", 1 );
	runptl();
	showdane();
	gnuplotmake();

	gnuplotend();
}

/*-------------------------------------------*/

void test6( void ) {

	int i = 6;

	mkdirworkdir_t( nrtest++ );
	
	colortbl[4] = 0xaaaa00;
	colortbl[5] = 0x00aa00;
	
	gnuplotstart();
	elemit( i );
	
	wskfun = nextrc1;
	sprintf( namefilter, "1xRC_%uT", i );
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextrc2;
	sprintf( namefilter, "2xRC_%uT", i );
	runptl();
	showdane();
	gnuplotmake();

	wskfun = nextrc3;
	sprintf( namefilter, "3xRC_%uT", i );
	runptl();
	showdane();
	gnuplotmake();

	wskfun = nextrc3buf;
	sprintf( namefilter, "3xRC_buf_%uT", i );
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextselkey;
	sprintf( namefilter, "2th_sk_%uT", i );
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextrcsk;
	sprintf( namefilter, "3th_sk_%uT", i );
	runptl();
	showdane();
	gnuplotmake();
	
	wskfun = nextsk4;
	sprintf( namefilter, "4th_%uT", i );
	runptl();
	showdane();
	gnuplotmake();

	wskfun = nextrcsk;
	r1 /= 8;
	c1 *= 8;
	sprintf( namefilter, "3th_sk_%uT_tt8", i );
	runptl();
	showdane();
	gnuplotmake();
	
	elemit( i );
	wskfun = nextselkey;
	sprintf( namefilter, "sk_%uT_tt8_1.1", i );
	//c1 *= 0.99;
	//c2 *= 1;
	runptl();
	showdane();
	gnuplotmake();
	
	gnuplotend();	
}

/*-------------------------------------------*/

void test7( void ) {

	int i = 2;
	
	mkdirworkdir_t( nrtest++ );
	
	colortbl[0] = 0x00aa00;
	//gnuplotstartstr( "sk" );
	gnuplotstart();
	
	elemit( 2 );
	
	wskfun = nextrc2;
	sprintf( namefilter, "2xRC_10_%uT", 2 );
	elemaw( 10 );
	runptl();
	showdane();
	gnuplotmake();
	
	elemit( 7 );
	
	for( i=0; i<3; ++i ) {
		wskfun = nextselkey;
		sprintf( namefilter, "sk_7T_%4.2f_%4.2f", c2, r2 );
		runptl();
		showdane();
		gnuplotmake();
		c2 /= 1.05;
		r2 *= 1.1;
	}
	
	
	elemit( 2 );
	c1 *= 10;
	r1 /= 10;
	
	for( i=0; i<3; ++i ) {
		wskfun = nextrcsk;
		sprintf( namefilter, "3th_2T_%4.2f_%4.2f", c2, r2 );
		runptl();
		showdane();
		gnuplotmake();
		c2 *= 1.07;
		r2 *= 0.97;
	}

	elemit( 2 );
	wskfun = nextrc3buf;
	sprintf( namefilter, "3xRC_buf_%uT", i );
	runptl();
	showdane();
	gnuplotmake();

	
	gnuplotend();	
}

/*-------------------------------------------*/

void test8( void ) {
	
	double t;
	int i;
	
	mkdirworkdir_t( nrtest++ );
	gnuplotstart();
	t = 8;
	
	wskfun = next2xsk;
	elemit( t );
	
	for( i=0; i<3; ++i ) {
		sprintf( namefilter, "2x2th_%4.2fT_%4.2f", t, c3 );
		runptl();
		showdane();
		gnuplotmake();
		c3 *= 1.05;
	}
	
	wskfun = nextsk4;
	t = 8;
	elemit( t );
	
	for( i=0; i<1; ++i ) {
		sprintf( namefilter, "4th_%4.2fT_%4.2f", t, c3 );
		runptl();
		showdane();
		gnuplotmake();
	}
	
	elemawf( 0.95 );
	
	for( i=0; i<8; ++i ) {
		sprintf( namefilter, "4th_%4.2fT_%4.2f", t, c3 );
		runptl();
		showdane();
		gnuplotmake();
		elemawf( 1.02 );
	}
	
	
	gnuplotend();	
}

/*-------------------------------------------*/

int wczytajstr ( char * a, char * b, size_t s ) {

  size_t i = 0;
  if(b==NULL) return 0;

  /* skip white */
  while(1) {
    if(*b==0) return 0;
    if(*b>0x20) break;
    ++b;
  }
  
  while ( ++i < s ) {
    if ( *b == 0 ) break;
    *a++ = *b++;
  }

  *a = 0;

  return i - 1;
}

/*-------------------------------------------*/
