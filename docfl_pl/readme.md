## Analiza kilku filtrów uzyskując z sygnału PWM napięcie

W artykule zerkniemy na uzyskiwanie z sygnału PWM, sygnału analogowego (napięcie), a konkretnie na własności tegoż rozwiązania dla kilku filtrów RC.

PWM - jak sama nazwa mówi jest to współczynnik wypełnienia impulsów. Określa go takie cechy jak okres z jakim impulsy występują oraz długość tych impulsów. Ponieważ jest to PWM realny i generowany przez np mikrokontroler impulsy mają też napięcie.

Aby uzyskać napięcie, sygnał PWM podajemy na filtr. Cechy które nas ostatecznie interesują:
1. Czas ustalenia średniej napięcia wartości na wyjściu
2. Tętnienia przy ustalonej wartości

![lpf1xrc](img/filtry_1xrc.png)

Wyjściową bazą do rozeznania tematu będzie prosty filtr RC o stałej czasowej równej okresowi PWM, oraz napięciu 1V. Stała czasowa to iloczyn RC. Przykładowo, gdybyśmy mieli PWM o okresie 1ms (jedna milisekunda), to można dobrać wartości, r = 100kom, c=10nF.

Aby zrobić jakąś analize wyniku, będziemy liczyć wartość średnią z napięcia za okres PWM, oraz wartość tętnień w tym czasie. Interesuje nas więc, ile okresów potrzeba by napięcie od wartości 0 było dla nas satysfakcjonujące. Ja przyjąłem obliczenia dla wartości dziesiętnej. Tzn kiedy błąd będzie mniejszy niż kolejna cyfra. Dla 50% tak to określiłem:
- 1 cyfra gdy wynik >0.4
- 2 cyfra gdy wynik >0.49
- 3 cyfra gdy wynik >0.499
- itd
(przyjąłem 50%, z powodu że dla 50% są największe tętnienia)

W ten sposób dostaniemy wraz z każdym przybliżeniem wyniku czas w okresach jaki jest potrzebny do osiągnięcia rezultatu, oraz wartość tętnień w tym czasie. Oprócz tego aby porównać osiągnięte rezultaty licze współczynnik jakości. Jest to pierwiastek z iloczynu czasu oraz tętnień w relacji do filtru zwykłego RC dla zwykłego PWM. Dla filtru RC wynosi on oczywiście 1. Nie jest to wartość techniczna. Nie mówi czy coś się nadaje do zastosowania, ale mamy możliwość porównania jakości osiągniętego rezultatu.

Oto co się dzieje na wyjściu kiedy mając zero, ustawimy wypełnienie na 50%: Jak widać wartość średnia z każdym okresem zbliża się do zadanej wartości.

Przebieg:

![pwm](img/uc1rc.png)

- Uc1 = wartość bieżąca na kondensatorze C1
- Usr = wartość średnia napięcia na wyjściu za okres T
- Upwm = wartość napięcia na wyjściu PWM

I otrzymujemy wyniki: kolejne przybliżenie - czas w okresach - wartość tętnień

```1xrc
  1     2  0.2989
  2     5  0.2470
  3     7  0.2447
  4     9  0.2449
  5    12  0.2449
  6    14  0.2449
  7    16  0.2449
  8    18  0.2449
  9    21  0.2449
```

Wykresy z analizy:

![1rxt](img/d1xrc-tim.png) ![1rxpk](img/d1xrc-pk.png) ![t1rxtpk](img/d1xrc-tpk.png)

Przy stałej czasowej 1T tętnienia są raczej duże. W niektórych przypadkach może nawet nam coś takiego wystarczyć. A gdyby chcieć osiągnąć lepsze rezultaty? Prosty sposób to zwiększenie stałej czasowej filtra, kosztem jednak czasu jaki potrzebujemy na stabilizacje. Drugi sposób to dołożenie kolejnych stopni RC. A więc filtry wyższego rzędu. No więc zobaczmy następny etap.


## Filtr 2xRC oraz 3xRC są to filtry drugiego i trzeciego rzędu (po angielsku skróty 2th 3th)

![lpf2xrc](img/filtry_2xrc.png) ![lpf3xrc](img/filtry_3xrc.png)

Zaletami względem filtru RC, w naszym przypadku, jest redukcja tętnień kosztem jednak dłuższego czasu ustalania napięcia. Zalety to również prostota, a ewentualny wzmacniacz operacyjny robiący za bufor wyjściowy nie musi mieć nadzwyczajnych właściwości. Od razu porównamy na wykresie wszystkie 3 filtry RC z opcją zmiany stałej czasowej.

![axt](img/darc-tim.png) ![axpk](img/darc-pk.png) ![axtpk](img/darc-tpk.png)

Słabszy czas ustalenia napięcia w tych filtrach można poprawić przez zwiększenie impedancji kolejnego członu RC (większy R mniejsze C). Jednak zawsze pozostaje dylemat zbyt dużych rezystancji których nie można sobie zwiększać dowolnie. Dlatego wariacją może być filtr 3xRC z buforami. Czyli zastosowanie wzmacniaczy operacyjnych w rolach bufora separującego kolejne stopnie. Troche mało praktyczne z uwagi na ilość wzmacniaczy, ale lepsze rezultaty w zakresie ustalania czasu.

![brct](img/drxb-tim.png) ![brcpk](img/drxb-pk.png) ![brctpk](img/drxb-tpk.png)

## Filtr sallena-key'a drugiego rzędu

![skf](img/filtry_sk.png)

Porównanie tegoż filtra dla 3 różnych współczynników zmiany C2, z filtrem 2xRC

![skt](img/dsk-tim.png) ![skpk](img/dsk-pk.png) ![sktpk](img/dsk-tpk.png)

Jak widać lepszy czas ustalania wyniku w porównaniu do 2xRC. Wzmacniacz operacyjny w tym filtrze musi być lepszej jakości ze względu na bezpośrednie sprzężenie z sygnałem wejściowym przez rezystancje R1. Filtr sallena-key'a w zależności od doboru impedancji elementów RC oraz wzmocnienia, ma różne własności odpowiedzi impulsowej. Niewielkie (10-20%) zmniejszenie pojemności C2 prowadzi do oscylacji. W zależności od zastosowania plusem może być szybszy czas ustalenia wyniku.

Dla: R1=1.000    R2=1.200  C1=1.000    C2=0.833  

overshoot
```tim    value     pk-pk
  9  0.50028674 0.03136226
 17  0.49999980 0.03144019
 25  0.50000000 0.03144012
```

## Filtr trzeciego rzędu, sallena-key'a z filtrem RC na wejściu

![skf](img/filtry_rcsk.png)

Filtr lepszy od poprzednika, w zakresie tętnień, jak i w fakcie że sprzężenie z sygnałem wejściowym jest odseparowane przez filr RC na wejściu, co zmniejsza wymagania odnośnie wzmacniacza. Jednak występuje tutaj sytuacja podobna co w filtrze 2xRC, że pierwszy człon R1C1 jest obciążony przez kolejny. Mniejsza rezystancja R1 w relacji do R2 daje lepsze rezultaty w zakresie ustalania wyniku. Pamiętając jednak o zmniejszeniu C1 jeśli chcemy zachować stałą czasową.

Porównanie efektów z trzema stałymi czasowymi 1,3,9 plus ostatni ze mniejszą impedancją pierwszego stopnia RC.

![rcskt](img/drcsk-tim.png) ![rcskpk](img/drcsk-pk.png) ![rcsktpk](img/drcsk-tpk.png)

## Filtr 4th_1xOPAMP, czwartego rzędu na jednym wzmacniaczu operacyjnym

![skf](img/filtry_4th_1opamp.png)

Zdecydowałem się na sprawdzenie filtru czwartego rzędu na pojedynczym wzmacniaczu operacyjnym. Zaczniemy od wad, długi czas stabilizacji wyniku, zalety to mniejsze tętnienia oraz fakt użycia jednego wzmacniacza. Także i tutaj zmniejszanie pojemności wraz z kolejnym stopniem RC skutkuje na pewnym etapie oscylacjami, wraz ze zmniejszeniem czasu.

Porównanie efektów z trzema stałymi czasowymi 1,3,9

![4th1t](img/d4th1-tim.png) ![4th1pk](img/d4th1-pk.png) ![4th1tpk](img/d4th1-tpk.png)

## Filtr podwójny sallen-key

![2xskf](img/filtry_2xsk.png)

Całkiem krótki czas ustalania plus małe tętniena. Porównanie efektów z trzema stałymi czasowymi 1,3,9

![2skt](img/d2xsk-tim.png) ![2skpk](img/d2xsk-pk.png) ![2sktpk](img/d2xsk-tpk.png)

## Wszystkie filtry ze stałą czasową 3:

![sumt](img/dsum-tim.png) ![sumpk](img/dsum-pk.png) ![sumtpk](img/dsum-tpk.png)

Pamiętajcie zawsze o błędach.
1. Napięcie z którego powstaje PWM
2. Obciążenia filtrów innych obwodów, źródła PWM.
3. Tolerancja wartości elementów
4. Wszelakie błedy wzmacniaczy operacyjnych
5. Dryfty termiczne elementów
6. W szczególnych przypadkach dokładność samego zegara PWM

