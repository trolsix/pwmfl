Simulator program to analize low pass filter for PWM signal filtering.
Calculate time to settings digits precision, peak to peak voltage, and performance filtered signal.

Also can calculate overshoot. Maximal precision is for 9 digits.

If gnuplot is in system, can plotting several data charts for visual and compare results.
In default path /tmp/pwmfl save data, and make html file.
